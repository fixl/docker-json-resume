# JSON Resume Docker Image

[![pipeline status](https://gitlab.com/fixl/docker-json-resume/badges/master/pipeline.svg)](https://gitlab.com/fixl/docker-json-resume/-/pipelines)
[![version](https://fixl.gitlab.io/docker-json-resume/version.svg)](https://gitlab.com/fixl/docker-json-resume/-/commits/master)
[![size](https://fixl.gitlab.io/docker-json-resume/size.svg)](https://gitlab.com/fixl/docker-json-resume/-/commits/master)
[![Docker Pulls](https://img.shields.io/docker/pulls/fixl/json-resume)](https://hub.docker.com/r/fixl/json-resume)
[![Docker Stars](https://img.shields.io/docker/stars/fixl/json-resume)](https://hub.docker.com/r/fixl/json-resume)

A Docker container that runs your [json-resume](hhttps://www.npmjs.com/package/resume-cli) cli.

## JSON Resume

Json resume lets you create json-based resumes easily.

## Build the image

```
make build
```

## Usage

Build your resume:
```bash
docker run --rm -it -v $(pwd):/src fixl/json-resume export --theme kendall --format html resume.html
```

This will generated a html version of your host that you can easily host.

Another option is to build a pdf version:
```bash
docker run --rm -it -v $(pwd):/src fixl/json-resume export --theme kendall --format pdf resume.pdf
```
